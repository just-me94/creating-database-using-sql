CREATE TABLE player (
    consultant_ID integer,
    Player_name varchar,
    Team_ID integer,
	Group_ID integer,
	PRIMARY KEY (consultant_ID)
);

CREATE TABLE card (
    card_ID integer,
    match_ID integer,
    consultant_ID integer,
	colour_ID integer,
	PRIMARY KEY (card_ID)
);

CREATE TABLE colour(
	colour_ID integer,
	colour varchar,				
	PRIMARY KEY (colour_ID)
);

CREATE TABLE goal(
	goal_ID integer,
	match_id integer,
	consultant_ID integer
	PRIMARY KEY (goal_ID)
);

CREATE TABLE group(
	group_ID integer,
	group_name varchar,
	PRIMARY KEY (group_ID)
);

CREATE TABLE home(
	home_ID integer,
	home_or_away varchar,
	PRIMARY KEY (home_ID)
);

CREATE TABLE matches(
	match_ID integer,
	dates date,
	venue_ID integer,
	PRIMARY KEY (match_ID)
);

CREATE TABLE results (
    results_ID integer,
    match_ID varchar,
    team_ID integer,
	home_ID integer,
	team_goals integer
	opposition_goals integer,
	points integer,
	PRIMARY KEY (results_ID)	
);

CREATE TABLE team(
	team_ID integer,
	team_name varchar,				
	PRIMARY KEY (team_ID)
);

CREATE TABLE venue(
	venue_ID integer,
	venue varchar,				
	PRIMARY KEY (venue_ID)
);

INSERT INTO venue(venue_ID,venue)
VALUES ('Wimbledon 1', 'Wimbledon 2', 'Wimbledon 3');


